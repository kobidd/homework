@extends('layouts.app')
@section('content')
<h1>This is your book list</h1>
<table>
      <tr>
      <th>Title</th>
      <th>Author</th>
      </tr>
      @foreach($books as $book)
       <tr>
       <td>
       @if ($book->status )
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif
       @cannot('reader')<a href = "{{route('books.edit', $book->id)}}">@endcannot{{$book->title}}</td>
       <td> @cannot('reader')<a href = "{{route('books.edit', $book->id)}}">@endcannot{{$book->author}}</td>
    </tr>
    @endforeach
</table>
@cannot('reader')<h1>Create New Book</h1 >
<a href = "{{route('books.create')}}"> Create a Book</a>@endcannot
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               console.log(event.target.id)
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'PUT',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}) ,
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>
@endsection