@extends('layouts.app')
@section('content')
<h1>Edit Book</h1>
<form method = 'post' action="{{action('BooksController@update', $book->id )}}">
@csrf 
@method('PATCH')
<div class = "form-group">
    <label for = "title">Book Name</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$book->title}}">
    <br>
    <label for = "author">Author Name</label>
    <input type= "text" class = "form-control" name= "author" value = "{{$book->author}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Edit Book">
</div>

</form>
<h1>Delete Book</h1>
<form method = 'post' action="{{action('BooksController@destroy', $book->id )}}">
@csrf 
@method('DELETE')
<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "Delete Book">
    </div>
</form>
@endsection