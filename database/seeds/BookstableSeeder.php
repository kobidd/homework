<?php

use Illuminate\Database\Seeder;

class BookstableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                        'title' => 'The Hunger Games',
                        'author' => 'Suzanne Collins',
                        'created_at' => date('Y-m-d G:i:s'),
                        'user_id' => 1,
                ],
                [
                        'title' => 'Game Of Thrones',
                        'author' => 'Jeorge RR Martin',
                        'created_at' => date('Y-m-d G:i:s'),
                        'user_id' => 2,
                ],
                [
                        'title' => 'To Kill a Mockingbird',
                        'author' => 'Harper Lee',
                        'created_at' => date('Y-m-d G:i:s'),
                        'user_id' => 1,
                ],
                [
                        'title' => 'Pride and Prejudice',
                        'author' => 'Jane Austen',
                        'created_at' => date('Y-m-d G:i:s'),
                        'user_id' => 3,
                ],
                [
                       'title' => 'The Book Thief',
                       'author' => 'Markus Zusak',
                       'created_at' => date('Y-m-d G:i:s'),
                       'user_id' => 2,
                ]
                
            
                    ]);
            
    }
}